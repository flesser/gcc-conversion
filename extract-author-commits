#! /bin/bash

# Given output from compare-authors on standard input, extract commits
# and log messages, from the repository named as $1, into the
# directory $2, and try to classify them.

set -e

repo_dir=$1
out_dir=$2

while read -r line1; do
    case "$line1" in
	("# Commit: "*)
	    read -r line2
	    read -r line3
	    commits="${line1#*Commit: }"
	    svnid="${line2#*Authors (SVN }"
	    svnid="${svnid%%)*}"
	    for commit in $commits; do
		(cd "$repo_dir" && git diff "$commit^-" >> "$out_dir/$svnid-diffs")
		(cd "$repo_dir" && git show --pretty='%B' -s "$commit" >> "$out_dir/$svnid-logs")
	    done
	    type=unknown
	    if grep -q -a '^diff .*ChangeLog' "$out_dir/$svnid-diffs"; then
		if grep -a '^diff ' "$out_dir/$svnid-diffs" | grep -q -v -a ChangeLog; then
		    if grep -q -i -a backport "$out_dir/$svnid-diffs" || grep -q -i -a backport "$out_dir/$svnid-logs"; then
			type=backport
		    fi
		else
		    type=logonly
		fi
	    else
		type=nolog
		# Attempt to extract an author from the commit
		# message.  If a single author is extracted that way,
		# this is probably a safe case.  Leave cases with
		# possible authors in commit messages as comments for
		# further inspection unless they exactly match the
		# git-svn authors, in which case they are probably
		# safe.
		tmp=$(mktemp)
		grep -a '@' "$out_dir/$svnid-logs" | grep -v '^[ 	]' |sed -e 's/ \{1,\}/ /g' > "$tmp"
		if [ -s "$tmp" ]; then
		    type=nolog-good
		    poss_author=$(sort -u < "$tmp")
		    poss_author=$(echo "$poss_author" | sed -e 's/^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] //' | sed -e 's/.*[0-9][0-9][0-9][0-9] //')
		    poss_author="'$svnid': '$poss_author',"
		    if [ "$poss_author" != "$line3" ]; then
			sed -e 's/^/# /' < "$tmp" >> "$out_dir/AUTHORS-$type"
		    fi
		fi
		rm -f "$tmp"
	    fi
	    printf '%s\n%s\n  %s\n' "$line1" "$line2" "$line3" \
		   >> "$out_dir/AUTHORS-$type"
	;;
	(*)
	    echo "Bad line $line1" >&2
	    exit 1
	    ;;
    esac
done
