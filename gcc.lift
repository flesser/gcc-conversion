# The GCC conversion recipe
#

set echo

authors read <gcc.map

# Deal with the one ambiguous username.
(1..<15891> & /master/b) | /premerge-fsf-branch/b assign gcc2
<gcc2> authors read <<EOF
dje = Doug Evans <dje@gnu.org>
EOF
~<gcc2> authors read <<EOF
dje = David Edelsohn <dje@gcc.gnu.org> America/New_York
dje-logonly1 = David Edelsohn <dje@gmail.com> America/New_York
EOF

# Process the ChangeLog files, including ChangeLog.<branch>, for
# better commit attribution.  However, don't do this for merges as
# those may contain many different ChangeLog entries for changes being
# merged.  It would probably be better to do this for branch formation
# as well, but forming a selection set for this is not as simple.
~=M changelogs /^ChangeLog(\..*)?$/

# If anything after this fails, it's better to get a diagnostic
# message than have the script abort
set relax

# This commit was made by Julian Brown with his own email, but Paul's name.
# (Reviewing the email logs suggest this is the correct attribution.)
<98732> attribution =A set paul@codesourcery.com

# Commit made by Dale Johannesen with his own email, but Geoff Keating's name.
<98100> attribution =A set geoffk@apple.com

# Commit made by Aldy Hernandez with his own email, but Richard
# Henderson's name.
<170360> attribution =A set rth@redhat.com

# Correct parents of branch-creation commits where cvs2svn messed up
# identifying the right parent commit.

# /branches/GC_5_0_ALPHA_1
<27855>|<27860> reparent --use-order
# /branches/apple-200511-release-branch
<105446>|<105574> reparent --use-order
# /branches/apple-gcc_os_35-branch
<90334>|<90607> reparent --use-order
# /branches/apple-tiger-release-branch
<96593>|<96595> reparent --use-order
# /branches/bje-unsw-branch
<97590>|<97591> reparent --use-order
# /branches/bounded-pointers-branch
<33062>|<33333> reparent --use-order
# /branches/cfg-branch
<46941>|<46945> reparent --use-order
# /branches/csl-3_3_1-branch
<70142>|<70143> reparent --use-order
# /branches/csl-3_4-linux-branch
<90109>|<90110> reparent --use-order
# /branches/csl-3_4_0-hp-branch
<80842>|<80843> reparent --use-order
# /branches/csl-3_4_3-linux-branch
<92959>|<93879> reparent --use-order
# /branches/csl-arm-2004-q3-branch
<90933>|<90934> reparent --use-order
# /branches/csl-gxxpro-3_4-branch
<102441>|<102442> reparent --use-order
# /branches/csl-sol210-3_4-branch
<87903>|<87927> reparent --use-order
# /branches/cygming331
<70142>|<70683> reparent --use-order
# /branches/cygming332
<73013>|<73014> reparent --use-order
# /branches/cygwin-mingw-gcc-3_1-branch
<53596>|<53609> reparent --use-order
# /branches/cygwin-mingw-gcc-3_2-branch
<55797>|<55799> reparent --use-order
# /branches/cygwin-mingw-gcc-3_2_1-branch
<59368>|<59662> reparent --use-order
# /branches/cygwin-mingw-v2-branch
<59267>|<60175> reparent --use-order
# /branches/egcs_1_00_branch
<16272>|<16282> reparent --use-order
# /branches/gcc-2_95_2_1-branch
<30160>|<30162> reparent --use-order
# /branches/gcc-3_2-branch
<55783>|<55785> reparent --use-order
# /branches/gcc-3_3-e500-branch
<65660>|<65902> reparent --use-order
# /branches/gcc-3_3-rhl-branch
<66832>|<66998> reparent --use-order
# /branches/gcc-3_4-e500-branch
<89410>|<89417> reparent --use-order
# /branches/gcc-3_4-rhl-branch
<80870.2>|<81014> reparent --use-order
# /branches/gcc-4_0-rhl-branch
<95655>|<95664> reparent --use-order
# /branches/gomp-01-branch
<62392>|<62579> reparent --use-order
# /branches/libgcj-2_95-branch
<27727>|<27730> reparent --use-order
# /branches/struct-reorg-branch
<86038>|<87007> reparent --use-order
# /branches/tree-cleanup-branch
<87698>|<87819> reparent --use-order
# /branches/apple-200508-beta-branch
<102940>|<102941> reparent --use-order
# /branches/bnw-simple-branch
<56620>|<56621> reparent --use-order
# /branches/egcs_gc_branch
<19615>|<19641> reparent --use-order
# /branches/ffixinc-branch
<23622>|<23624> reparent --use-order
# /branches/gcc-3_2-rhl8-branch
<56747>|<57454> reparent --use-order
# /branches/gnu-win32-b20-branch
<22523>|<22525> reparent --use-order
# /branches/structure-aliasing-branch
<86980>|<87042> reparent --use-order

# Likewise, for tag-creation commits.

# /tags/egcs_1_0_1_prerelease
<17184>|<17185> reparent --use-order
# /tags/egcs_1_0_1_release
<17282>|<17283> reparent --use-order
# /tags/egcs_1_0_2_980309_prerelease
<18445>|<18446> reparent --use-order
# /tags/egcs_1_0_2_prerelease
<18369>|<18370> reparent --use-order
# /tags/egcs_1_0_2_release
<18615>|<18616> reparent --use-order
# /tags/egcs_1_0_3_prerelease
<19381>|<19382> reparent --use-order
# /tags/egcs_1_0_3_release
<19763>|<19764> reparent --use-order
# /tags/egcs_1_0_release
<16925>|<16926> reparent --use-order
# /tags/egcs_1_1_1_pre
<23462>|<23463> reparent --use-order
# /tags/egcs_1_1_1_prerelease
<23486>|<23487> reparent --use-order
# /tags/egcs_1_1_1_prerelease_2
<23594>|<23595> reparent --use-order
# /tags/egcs_1_1_1_prerelease_3
<23824>|<23825> reparent --use-order
# /tags/egcs_1_1_1_release
<24055>|<24056> reparent --use-order
# /tags/egcs_1_1_2_prerelease_1
<25235>|<25236> reparent --use-order
# /tags/egcs_1_1_2_prerelease_2
<25404>|<25405> reparent --use-order
# /tags/egcs_1_1_2_prerelease_3
<25633>|<25634> reparent --use-order
# /tags/egcs_1_1_2_release
<25763>|<25764> reparent --use-order
# /tags/egcs_1_1_prerelease
<22097>|<22098> reparent --use-order
# /tags/egcs_1_1_release
<22147>|<22148> reparent --use-order
# /tags/gcc-2_95-release
<28337>|<28338> reparent --use-order
# /tags/gcc-2_95_1-release
<28721>|<28722> reparent --use-order
# /tags/gcc-2_95_2-release
<30160>|<30161> reparent --use-order
# /tags/gcc-2_95_2_1-release
<38098>|<38099> reparent --use-order
# /tags/gcc-2_95_3
<40552>|<40553> reparent --use-order
# /tags/gcc-2_95_3-test1
<38595>|<38596> reparent --use-order
# /tags/gcc-2_95_3-test2
<38946>|<38947> reparent --use-order
# /tags/gcc-2_95_3-test3
<39265>|<39266> reparent --use-order
# /tags/gcc-2_95_3-test4
<39881>|<39882> reparent --use-order
# /tags/gcc-2_95_3-test5
<40409>|<40410> reparent --use-order
# /tags/gcc-2_95_test
<28255>|<28256> reparent --use-order
# /tags/gcc_3_0_1_release
<45039>|<45040> reparent --use-order
# /tags/gcc_3_0_2_release
<46437>|<46438> reparent --use-order
# /tags/gcc_3_0_3_release
<48212>|<48213> reparent --use-order
# /tags/gcc_3_0_4_release
<49906>|<49907> reparent --use-order
# /tags/gcc_3_0_release
<43430>|<43431> reparent --use-order
# /tags/gcc_3_1_1_release
<55765>|<55766> reparent --use-order
# /tags/gcc_3_2_1_release
<59267>|<59268> reparent --use-order
# /tags/gcc_3_2_2_release
<62430>|<62431> reparent --use-order
# /tags/gcc_3_2_3_release
<65931>|<65932> reparent --use-order
# /tags/gcc_3_2_release
<56289>|<56290> reparent --use-order
# /tags/gcc_3_3_1_release
<70142>|<70145> reparent --use-order
# /tags/gcc_3_3_2_release
<72568>|<72569> reparent --use-order
# /tags/gcc_3_3_3_release
<77825>|<77826> reparent --use-order
# /tags/gcc_3_3_4_release
<82513>|<82514> reparent --use-order
# /tags/gcc_3_3_5_release
<88339>|<88340> reparent --use-order
# /tags/gcc_3_3_6_release
<99149>|<99150> reparent --use-order
# /tags/gcc_3_3_release
<66791>|<66792> reparent --use-order
# /tags/gcc_3_4_0_release
<80842>|<80844> reparent --use-order
# /tags/gcc_3_4_1_release
<83995>|<83996> reparent --use-order
# /tags/gcc_3_4_2_release
<87128>|<87129> reparent --use-order
# /tags/gcc_3_4_3_release
<90109>|<90112> reparent --use-order
# /tags/gcc_3_4_4_release
<99964>|<99965> reparent --use-order
# /tags/gcc_4_0_0_release
<98491>|<98492> reparent --use-order
# /tags/gcc_4_0_1_release
<101726>|<101728> reparent --use-order
# /tags/gcc-2_8_1-RELEASE
<18394>|<18395> reparent --use-order
# /tags/gcc_3_1_release
<53469>|<54955> reparent --use-order
# /tags/gcc_4_0_2_release
<104479>|<104510> reparent --use-order
# /tags/libgcj-2_95-release
<28379>|<28380> reparent --use-order
# /tags/libgcj-2_95_1-release
<28801>|<28802> reparent --use-order

# Avoid merge commits on master.
# cxx0x-lambdas-branch
<152318> unmerge
# lto
<152434> unmerge
# wide-int
<210113> unmerge

# Ensure libstdcxx_so_7-2-branch (created by creating directory then
# copying subdirectory) is parented on relevant master commit.
<183775>|<183779> reparent --use-order

# Deletion of spurious commits.

# Commit 243505 wrongly created a branch as a copy of / not /trunk; it
# was then deleted.
<243505>,<243547> delete

# Try to generate better commit summary lines.
msgout >gcc-commitlog
shell ./fixbugmessages
msgin <gcc-fixedprs

# Strata tags.
tag misc/cutover-git create <280156>
msgin <<EOF
Tag-Name: misc/cutover-git
Tagger: Joseph Myers <jsm28@gcc.gnu.org>

This tag marks the last Subversion commit before the move to Git.
EOF

tag misc/cutover-egcs-0 create <14639>
msgin <<EOF
Tag-Name: misc/cutover-egcs-0
Tagger: Joseph Myers <jsm28@gcc.gnu.org>

This is the snapshot of the gcc2 CVS repository on which EGCS was
based.  Subsequent changes in the gcc2 CVS repository are on
premerge-fsf-branch.  That repository started out as a collection of
RCS files, and initially not all files were version-controlled, so the
gcc2 history does not contain a complete buildable set of source
files; in particular, documentation and ChangeLogs were not
version-controlled until late in gcc2 history.
EOF

tag misc/cutover-egcs-1 create <14764>
msgin <<EOF
Tag-Name: misc/cutover-egcs-1
Tagger: Joseph Myers <jsm28@gcc.gnu.org>

This was the initial commit of the EGCS fork.  Later revisions follow
that fork, which became GCC 3.

This is the first point at which the repository has complete coverage
of the code tree.
EOF

tag misc/cutover-cvs2svn create <105925>
msgin <<EOF
Tag-Name: misc/cutover-cvs2svn
Tagger: Joseph Myers <jsm28@gcc.gnu.org>

This is the point at which cvs2svn was applied to move the history
from CVS to Subversion.  A custom version of CVS was used to combine
the files from the gcc2 CVS repository with those from the EGCS /
GCC 3 CVS repository before the result was converted with cvs2svn.
EOF

# Tags for ancient releases. See https://gcc.gnu.org/releases.html
# for a list of ancient releases without tarballs; this one
# is certainly not complete.  These are from ssb.
#define release {0} reset refs/tags/{1} create
#do release <3> 1.36
#do release <358> 2.0
#do release <586> 2.1
#do release <1184> 2.2
#do release <2674> 2.3.1
# r4489 + r4492 + r4493  = 2.4.0
#do release <5867> 2.5.0
#do release <7771> 2.6.0
#do release <9996> 2.7.0
#do release <10608> 2.7.2

# To be done: incorporate tarballs from ancient releases
# Source tarballs are at:
# ftp://sourceware.org/pub/gcc/old-releases/gcc-1/
# ftp://sourceware.org/pub/gcc/old-releases/gcc-2/

lint >gcc-lint

# Massage comments into gittish form with separator lines where it can be
# done automatically.
gitify

# Add the version of SVN that this commit was copied from
=C append --rstrip --legacy "\n\nFrom-SVN: r%LEGACY%\n"

# Statistics on read and processing times
timing
